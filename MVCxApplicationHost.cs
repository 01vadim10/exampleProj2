﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xafari.Mvc;
using Xafari.Mvc.MVCx;

namespace Galaktika.HCM.Mvc
{
	public class HCMMVCxApplicationHost : XafariMVCxApplicationHost
	{
		public HCMMVCxApplicationHost()
		{
			this.UseSharedApplication = false;
			this.SwitchToNewStyle = false;
		}

		protected override void OnApplicationCreated(MVCxApplication application)
		{
			base.OnApplicationCreated(application);
			// add here code to customize application after it is created
		}

		protected override void OnApplicationInitialized(MVCxApplication application)
		{
			base.OnApplicationInitialized(application);
			// add here code to customize application after it is initialized by XafariApplicationContext
		}

		protected override void OnApplicationStarted(MVCxApplication application)
		{
			base.OnApplicationStarted(application);
			// add here code to customize application after it is started
		}

		protected override void SetupCore()
		{
			base.SetupCore();
			// add here code to customize application environmemt
		}

		protected override void RegisterGlobalFilters(GlobalFilterCollection globalFilterCollection)
		{
			globalFilterCollection.Add(new HandleErrorAttribute());
		}

		protected override void RegisterRoutes(RouteCollection routeCollection)
		{
			routeCollection.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routeCollection.IgnoreRoute("{resource}.ashx/{*pathInfo}");

			XafariRouteConfig.Instance.IgnoreRoutes(routeCollection);

			XafariRouteConfig.Instance.RegisterRoutes(routeCollection);
		}

		protected override void RegisterAuth()
		{
			// To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
			// you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

			//OAuthWebSecurity.RegisterMicrosoftClient(
			//    clientId: "",
			//    clientSecret: "");

			//OAuthWebSecurity.RegisterTwitterClient(
			//    consumerKey: "",
			//    consumerSecret: "");

			//OAuthWebSecurity.RegisterFacebookClient(
			//    appId: "",
			//    appSecret: "");

			//OAuthWebSecurity.RegisterGoogleClient();
		}

		protected override void HandleExceptionCore(MVCxApplication application, Exception exception)
		{
			base.HandleExceptionCore(application, exception);
			// add here code to customize exception handling
		}
	}
}