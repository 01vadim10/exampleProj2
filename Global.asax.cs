﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DevExpress.Persistent.Base;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using Xafari.Mvc;
using Xafari.Mvc.Infrastructure;
using Xafari.Mvc.MVCx;
using Xafari.Web.Utils;

namespace Galaktika.HCM.Mvc
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			HCMMVCxApplicationHost.SetInstance(new HCMMVCxApplicationHost()).Setup();
		}

		protected void Session_Start(Object sender, EventArgs e)
		{
			HCMMVCxApplicationHost.Instance.StartApplication(Session);
		}

		protected void Session_End(Object sender, EventArgs e)
		{
			HCMMVCxApplicationHost.DisposeApplication(Session);
		}

		protected void Application_End(Object sender, EventArgs e)
		{
			HCMMVCxApplicationHost.DisposeInstance();
		}

		protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
		{
			HCMMVCxApplicationHost.PreRequestHandler();
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			HCMMVCxApplicationHost.HandleException();
		}
	}
}