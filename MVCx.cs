﻿using JetBrains.Annotations;

[assembly: AspMvcViewLocationFormat("~/Views/MVCx/{1}/{0}.cshtml")]
[assembly: AspMvcViewLocationFormat("~/Views/MVCx/{1}/{0}.vbhtml")]
[assembly: AspMvcViewLocationFormat("~/Views/MVCx/{1}/{0}.ascx")]
[assembly: AspMvcViewLocationFormat("~/Views/MVCx/{1}/{0}.aspx")]

[assembly: AspMvcPartialViewLocationFormat("~/Views/MVCx/Shared/{0}.cshtml")]
[assembly: AspMvcPartialViewLocationFormat("~/Views/MVCx/Shared/{0}.vbhtml")]
[assembly: AspMvcPartialViewLocationFormat("~/Views/MVCx/Shared/{0}.ascx")]
[assembly: AspMvcPartialViewLocationFormat("~/Views/MVCx/Shared/{0}.aspx")]

[assembly: AspMvcMasterLocationFormat("~/Views/MVCx/Shared/{0}.cshtml")]
[assembly: AspMvcMasterLocationFormat("~/Views/MVCx/Shared/{0}.vbhtml")]
[assembly: AspMvcMasterLocationFormat("~/Views/MVCx/Shared/{0}.ascx")]
[assembly: AspMvcMasterLocationFormat("~/Views/MVCx/Shared/{0}.aspx")]
